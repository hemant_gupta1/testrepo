package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerDemo {
	
	@Value("${message}")
	private String message;
	
	@GetMapping("/hello")
	public String greeting() {
		return message;
	}
	
	@GetMapping("/evenList")
	public List getEvenList(@RequestBody ArrayList<Integer> numberList) {
		Predicate<Integer> p = i->i%2==0;
	    return numberList.stream().filter(n -> p.test(n)).collect(Collectors.toList());
	}

}
